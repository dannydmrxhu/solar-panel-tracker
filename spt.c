#include <stdio.h>
#include <time.h>
#include "tsl2561.h"
#include "wxstn.h"
#include "panel.h"

extern positiondata_s positiontable[STMAXPSZ];

int main(void)
{
    int clux = 0;
    reading_s readings = {0};
    ldrsensor_s cldr = {0};
    //int azpos[19] = {120, 110, 100, 90, 80, 70, 60, 50, 40, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120};
    //int elpos[19] = {150, 130, 110, 90, 110, 130, 150, 130, 110, 90, 110, 130, 150, 130, 110, 90, 110, 130, 150};
    int az[11] = {180,165,150,135,120,105,90,75,60,45,30};
    int el[11] = {180,170,160,150,140,130,120,110,100,90,90};

    panelpos_s npos = {0};
    int i = 0;
    int j = 0;
    int astart, aend, estart, eend;
    double arange, erange;
    StPanelInitialization();

    printf("CENG252 Panel Tracker\n");

    for(i=0; i<11; i++)
    {
        npos.Azimuth = az[i];
        npos.Elevation = el[i];
        StSetPanelPosition(npos);
        WsDelay(3000);
    }

    for(i=0; i<11; i++)
    {
        npos.Azimuth = az[i];
        npos.Elevation = el[i];
        StSetPanelPosition(npos);
        printf("Az: %3d - SPWM: %4d  El: %3d HPWM: %4d\n",az[i],positiontable[az[i]].spwm,el[i],positiontable[el[i]].hpwm);
        do
        {
            printf("Enter New SPWM: ");
            scanf("%4d",&j);
            if(j == -1) break;
            positiontable[az[i]].spwm = j;
            StSetPanelPosition(npos);
        } while(1);
        do
        {
            printf("Enter New HPWM: ");
            scanf("%4d",&j);
            if(j == -1) break;
            positiontable[el[i]].hpwm = j;
            StSetPanelPosition(npos);
        } while(1);
    }
    StSavePositionTable();

        for(i=30; i<STMAXPSZ; i+=15)
    {
        astart = i;
        aend = i+15;
        arange = positiontable[aend].spwm - positiontable[astart].spwm;
        for(j=1; j <=15; j++)
        {
            positiontable[astart+j].spwm = (j*arange/15.0)+positiontable[astart].spwm+.5;
        }

    }

    for(i=90; i<STMAXPSZ; i+=10)
    {
        astart = i;
        aend = i+10;
        arange = positiontable[eend].hpwm - positiontable[estart].hpwm;
        for(j=1; j <=10; j++)
        {
            positiontable[estart+j].hpwm = (j*arange/10.0)+positiontable[estart].hpwm+.5;
        }

    }


    return 1;


    //printf("CENG252 Panel Tracker\n");

   // while(1)
    //{
       // readings = WsGetReadings();
      //  clux = tsl2561GetLux();
     //   WsDisplayReadings(readings);
       // tsl2561DisplayLux(clux);
      //  cldr = StGetLdrReadings();
       // StDisplayLdrReadings(cldr);
        //npos.Azimuth = azpos[i];
        //npos.Elevation = elpos[i];
        //printf("AA: %3d EA: %3d\n", azpos[i], elpos[i]);
        //StSetPanelPosition(npos);
        //npos = StGetPanelPosition();
        //printf("Azimuth: %d Elevation: %d\n", azpos[i], elpos[i]);
        //printf("Azimuth: %3.0lf Elevation: %3.0lf\n", npos.Azimuth, npos.Elevation);
        //i++;
        //if(i > 18) {i=0;}
       // WsDelay(5000);
   // }

    //return 1;
}
